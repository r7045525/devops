# Архитектура проекта retail.ru

## Файловая структура
```
retail.ru
├── api-docs
│   ├── .env
│   └── front-end
├── back-end
│   └── .gitkeep
├── environment
│   ├── balancer
│   │   └── default.conf
│   ├── .env
│   ├── docker-compose.yml
│   ├── Makefile
│   └── README.md
└── front-end
    ├── front
    │   ├── public
    │   ├── server
    │   ├── .gitignore
    │   ├── app.vue
    │   ├── nuxt.config.ts
    │   ├── package.json
    │   ├── README.md
    │   ├── tsconfig.json
    │   └── yarn.lock
    ├── .gitignore
    ├── Dockerfile
    └── README.md
```

* retail.ru:
  * Папка `api-docs` — исходные файлы проекта
  * Папка `back-end` — каталог с архивами образов предыдущих релизов
  * Папка `environment` — каталог с архивами образов предыдущих релизов
  * Папка `front-end` — каталог с архивами образов предыдущих релизов
 