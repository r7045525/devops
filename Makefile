#!/usr/bin/env make
#
#ifneq (,$(wildcard ./.env))
#    include .env
#    export
#endif

default:
	@echo "make needs target:"
	@egrep -e '^\S+' ./Makefile | grep -v default | sed -r 's/://' | sed -r 's/^/ - /'
# Запуск приложения
up:
	docker-compose up -d
# Останов приложения
down:
	docker-compose down
# Рестарт контейнера
restart:
	docker-compose stop
	docker-compose start
# Перенастройка работающего балансировщика на green-апстрим
green:
	docker-compose exec balancer sed -i 's/techlab.rarus.ru-ssr-blue;/techlab.rarus.ru-ssr-green;/' /etc/nginx/conf.d/default.conf
	docker-compose exec balancer nginx -t
	docker-compose exec balancer nginx -s reload
# Перенастройка работающего балансировщика на blue-апстрим
blue:
	docker-compose exec balancer sed -i 's/techlab.rarus.ru-ssr-green;/techlab.rarus.ru-ssr-blue;/' /etc/nginx/conf.d/default.conf
	docker-compose exec balancer nginx -t
	docker-compose exec balancer nginx -s reload
# диагностика образа
bash:
	docker-compose exec balancer sh $(filter-out $@,$(MAKECMDGOALS))
%:
	@: # silence